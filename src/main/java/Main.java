import Model.Algorithm.CGeneticAlgorithmService;
import Model.Data.CData;
import Model.Data.CResult;
import Model.Operator.*;
import Model.Phenotype.CGenerateGenotype;
import Model.Phenotype.IGenerateGenotype;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import org.knowm.xchart.style.Styler;
import org.knowm.xchart.style.markers.SeriesMarkers;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    final private static int POP_SIZE = 1000;
    final private static double CROSS_PROB = 0.5;
    final private static double MUT_PROB = 0.3;
    final private static int MAX_GEN = 500;

    final private static IGenerateGenotype GENERATE_GENOTYPE = new CGenerateGenotype();
    final private static ISelectOperator SELECT_OPERATOR = new CSelectOperatorTurniej(2);
    final private static ICrossoverOperator CROSSOVER_OPERATOR = new CCrossoverOperatorV3(6);
    final private static IMutateOperator MUTATE_OPERATOR = new CMutateOperator(3);

    final private static int REPEAT_ALGORITHM = 25;

    final private static String RESULT_FILE_NAME = "wynik.csv";


/*    final private static String FILE_NAME = "data/had12.dat";
    final private static int OPT = 1652;*/


/*    final private static String FILE_NAME = "data/had14.dat";
    final private static int OPT =  2724;*/

/*    final private static String FILE_NAME = "data/had16.dat";
    final private static int OPT =  3720;*/

/*    final private static String FILE_NAME = "data/had18.dat";
    final private static int OPT =  5358;*/

    final private static String FILE_NAME = "data/had20.dat";
    final private static int OPT =  6922;

    public static void main(String[] args) throws IOException {
        CData data = CData.loadData(FILE_NAME);
        System.out.println(data);


        CGeneticAlgorithmService geneticAlgorithmService = new CGeneticAlgorithmService(
                POP_SIZE,
                CROSS_PROB,
                MUT_PROB,
                MAX_GEN,
                data,
                GENERATE_GENOTYPE,
                SELECT_OPERATOR,
                CROSSOVER_OPERATOR,
                MUTATE_OPERATOR
        );

        List<CResult> resultList = new ArrayList<>(REPEAT_ALGORITHM);

        for (int i = 0; i < REPEAT_ALGORITHM; i++) {
            geneticAlgorithmService.run();
            resultList.add(geneticAlgorithmService.getResult());
        }

        List<List<CResult.SStatistic>> statisticList =
                resultList
                        .stream()
                        .map(CResult::getStatisticList)
                        .collect(Collectors.toList());


/*        new File(RESULT_FILE_NAME).delete();
        resultList.forEach(Main::saveResult);  */

        List<CResult.SStatistic> mergedStatistic = mergeStatistic(statisticList);
        showChart(mergedStatistic);
    }

    private static void saveResult(CResult result) {
        try (BufferedWriter bufferedWriter=new BufferedWriter(new FileWriter(RESULT_FILE_NAME,true))) {
            for (CResult.SGenerationStatistic generationStatistic:result.getGenerationStatistics()){
                bufferedWriter.write(generationStatistic.generation);
                bufferedWriter.write(',');
                bufferedWriter.write(Long.toString(generationStatistic.time));
                for (int cost:generationStatistic.costArray){
                    bufferedWriter.write(',');
                    bufferedWriter.write(cost);
                }
                bufferedWriter.write('\n');

            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static List<CResult.SStatistic> mergeStatistic(List<List<CResult.SStatistic>> statisticMatrix) {
        List<CResult.SStatistic> mergedStatistic = new LinkedList<>();
        CResult.SStatistic currentProcessStatistic;

        long min = 0;
        long max = 0;
        long avg = 0;
        long time = 0;

        int size = statisticMatrix.size();

        for (int i = 0; i < statisticMatrix.get(0).size(); i++) {
            min=max=avg=time=0;
            for (List<CResult.SStatistic> aStatisticMatrix : statisticMatrix) {
                currentProcessStatistic = aStatisticMatrix.get(i);
                min += currentProcessStatistic.minCost;
                max += currentProcessStatistic.maxCost;
                avg += currentProcessStatistic.avgCost;
                time += currentProcessStatistic.time;
            }
            mergedStatistic
                    .add(new CResult.SStatistic(
                            statisticMatrix.get(0).get(i).generation,
                            (int) min / size,
                            (int) max / size,
                            (int) avg / size,
                            (int) time/size
                    ));
        }

        return mergedStatistic;
    }

    private static void showChart(List<CResult.SStatistic> statisticList) {
        XYChart chart = new XYChartBuilder()
                .width(800)
                .height(600)
                .title("Wyniki")
                .xAxisTitle("Generacje")
                .yAxisTitle("Cost")
                .build();

        chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideNW);
        chart.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        chart.getStyler().setYAxisLabelAlignment(Styler.TextAlignment.Right);
        chart.getStyler().setYAxisDecimalPattern("###,###.##");
        chart.getStyler().setPlotMargin(0);
        chart.getStyler().setPlotContentSize(.95);


        XYChart chart2 = new XYChartBuilder()
                .width(400)
                .height(300)
                .title("Czas(Generacje)")
                .xAxisTitle("Generacje")
                .yAxisTitle("Czas[ms]")
                .build();

        chart2.getStyler().setLegendPosition(Styler.LegendPosition.InsideNW);
        chart2.getStyler().setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);
        chart2.getStyler().setYAxisLabelAlignment(Styler.TextAlignment.Right);
        chart2.getStyler().setYAxisDecimalPattern("###,###.##");
        chart2.getStyler().setPlotMargin(0);
        chart2.getStyler().setPlotContentSize(.95);



        int size = statisticList.size();

        int[] minY = new int[size];
        int[] maxY = new int[size];
        int[] avgY = new int[size];
        int[] genX = new int[size];
        int[] timeY = new int [size];

        int[] opt = new int[size];
        Arrays.fill(opt, OPT);

        int arraysIndex = 0;
        for (CResult.SStatistic statistic : statisticList) {
            minY[arraysIndex] = statistic.minCost;
            maxY[arraysIndex] = statistic.maxCost;
            avgY[arraysIndex] = statistic.avgCost;
            genX[arraysIndex] = statistic.generation;
            timeY[arraysIndex] = statistic.time;
            arraysIndex++;
        }

        for (CResult.SStatistic statistic : statisticList){
            System.out.format("%3d > %7d%n",statistic.generation,statistic.time);
        }

        chart.addSeries("minCost", genX, minY).setMarker(SeriesMarkers.NONE);
        chart.addSeries("maxCost", genX, maxY).setMarker(SeriesMarkers.NONE);
        chart.addSeries("avgCost", genX, avgY).setMarker(SeriesMarkers.NONE);
        chart.addSeries("opt", opt).setMarker(SeriesMarkers.NONE);

        chart2.addSeries("Czas",genX,timeY).setMarker(SeriesMarkers.NONE);

        new SwingWrapper<XYChart>(chart).displayChart();
        new SwingWrapper<XYChart>(chart2).displayChart();
    }
}
