package Model.Algorithm;

import Model.Data.CData;
import Model.Data.CResult;
import Model.Operator.ICrossoverOperator;
import Model.Operator.IMutateOperator;
import Model.Operator.ISelectOperator;
import Model.Phenotype.CCreature;
import Model.Phenotype.ICreature;
import Model.Phenotype.IGenerateGenotype;
import Model.Utils.RandomProvider;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

@Getter
public class CGeneticAlgorithmService {
    private final Random random = RandomProvider.getRandom();

    private final int populationSize;
    private final double crossProbability;
    private final double mutateProbability;
    private final int maxGenerations;
    private final CData data;

    private final IGenerateGenotype generateGenotype;
    private final ISelectOperator selectOperator;
    private final ICrossoverOperator crossoverOperator;
    private final IMutateOperator mutateOperator;

    private final CCreature.CICreatureFabric creatureFabric;
    private CResult result=null;

    public CGeneticAlgorithmService(int populationSize, double crossProbability, double mutateProbability, int maxGenerations, CData data, IGenerateGenotype generateGenotype, ISelectOperator selectOperator, ICrossoverOperator crossoverOperator, IMutateOperator mutateOperator) {
        this.populationSize = populationSize;
        this.crossProbability = crossProbability;
        this.mutateProbability = mutateProbability;
        this.maxGenerations = maxGenerations;
        this.data = data;
        this.generateGenotype = generateGenotype;
        this.selectOperator = selectOperator;
        this.crossoverOperator = crossoverOperator;
        this.mutateOperator = mutateOperator;

        this.creatureFabric = CCreature.CICreatureFabric.getCCreatureFabric(this.data);
    }


    public void run() {
        this.result = new CResult();
        long timeStartMillis = System.currentTimeMillis();
        List<ICreature> population = createNewPopulation();
        int generation = 0;
        saveStatistic(population, generation, System.currentTimeMillis()-timeStartMillis);

        ICreature bestCreature;
        while (generation < maxGenerations - 1) {
            bestCreature = getBestCreature(population);

            population = selectCreatures(population);
            population = crossCreatures(population);
            population = mutateCreatures(population);

            population.set(random.nextInt(populationSize), bestCreature);
            generation++;
            saveStatistic(population, generation, System.currentTimeMillis()-timeStartMillis);
        }
    }

    private ICreature getBestCreature(List<ICreature> population) {
        Optional<ICreature> creature = population.stream().min(Comparator.comparingInt(ICreature::getCost));
        if (creature.isPresent())
            return creature.get();
        else
            throw new NullPointerException();
    }

    private void saveStatistic(List<ICreature> population, int generation, long time) {
        int[] cosArray = new int[populationSize];
        int costArrayIndex = 0;
        for (ICreature creature : population) {
            cosArray[costArrayIndex++] = creature.getCost();
        }
        result.addStatistic(generation, cosArray, time);
    }

    private List<ICreature> createNewPopulation() {
        List<int[]> newPopulationGenotypes = new LinkedList<>();
        int genotypeSize = data.getMatrixSize();

        for (int genotypeNumber = 0; genotypeNumber < populationSize; genotypeNumber++) {
            newPopulationGenotypes.add(generateGenotype.generateGenotype(genotypeSize));
        }

        return newPopulationGenotypes.stream().map(creatureFabric::createNewICreature).collect(Collectors.toList());
    }

    private List<ICreature> selectCreatures(List<ICreature> population) {
        return selectOperator.selectCreatures(Collections.unmodifiableList(population));
    }

    private List<ICreature> crossCreatures(List<ICreature> population) {
        final List<ICreature> oldPopulation = new LinkedList<>(population);
        final List<ICreature> newPopulation = new LinkedList<>();
        while (!oldPopulation.isEmpty()) {
            ICreature parentOne = randAndRemoveParent(oldPopulation);
            ICreature parentTwo = randAndRemoveParent(oldPopulation);

            if (random.nextDouble() < crossProbability) {
                newPopulation.addAll(crossCreature(parentOne, parentTwo));
            } else {
                newPopulation.add(parentOne);
                newPopulation.add(parentTwo);
            }
        }

        return newPopulation;
    }

    private List<ICreature> crossCreature(ICreature parentOne, ICreature parentTwo) {
        final List<int[]> childrenGenotype = crossoverOperator.cross(parentOne.getGenotype(), parentTwo.getGenotype());
        return childrenGenotype.stream().map(creatureFabric::createNewICreature).collect(Collectors.toList());
    }

    private List<ICreature> mutateCreatures(List<ICreature> population) {
        final List<ICreature> newPopulation = new LinkedList<>();

        population.forEach(iCreature -> {
            if (random.nextDouble() < mutateProbability) {
                newPopulation.add(mutateCreature(iCreature));
            } else {
                newPopulation.add(iCreature);
            }
        });

        return newPopulation;
    }

    private ICreature mutateCreature(ICreature iCreature) {
        int[] newGenotype = mutateOperator.mutate(iCreature.getGenotype());
        return creatureFabric.createNewICreature(newGenotype);
    }

    private ICreature randAndRemoveParent(List<ICreature> parentList) {
        final int parentIndex = random.nextInt(parentList.size());
        ICreature parent = parentList.get(parentIndex);
        parentList.remove(parentIndex);
        return parent;
    }

}
