package Model.Operator;

import Model.Operator.Exception.DifferentGenotypeSizeException;
import Model.Utils.RandomProvider;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@NoArgsConstructor
public class CCrossoverOperatorOX implements ICrossoverOperator {
    protected final static int DEFAULT_CHAIN_SIZE = 2;
    protected final Random random = RandomProvider.getRandom();

    protected int chainSize = DEFAULT_CHAIN_SIZE;

    public CCrossoverOperatorOX(int chainSize) {
        this.chainSize = chainSize;
    }

    @Override
    public List<int[]> cross(int[] parentOneGenotype, int[] parentTwoGenotype) {
        if (parentOneGenotype.length != parentTwoGenotype.length)
            throw new DifferentGenotypeSizeException();

        final int subChainEnd = random.nextInt(parentOneGenotype.length - chainSize) + chainSize;
        final int subChainStart = subChainEnd - chainSize;
        List<int[]> childrenGenotype = new LinkedList<>();
        childrenGenotype.add(crossAlgorithm(parentOneGenotype, parentTwoGenotype, subChainStart, subChainEnd));
        childrenGenotype.add(crossAlgorithm(parentTwoGenotype, parentOneGenotype, subChainStart, subChainEnd));
        return childrenGenotype;
    }


    protected int[] crossAlgorithm(int[] genotype1, int[] genotype2, int subChainStart, int subChainEnd) {

        final int genesLength = genotype1.length;
        final int[] newGenes = new int[genotype1.length];

        final boolean[] usedGenes = new boolean[genesLength];
        int genFromGenes1;

        for (int newGenesIndex = subChainStart; newGenesIndex < subChainEnd; newGenesIndex++) {
            genFromGenes1 = genotype1[newGenesIndex];
            newGenes[newGenesIndex] = genFromGenes1;
            usedGenes[genFromGenes1 - 1] = true;
        }

        int genes2Index = 0;
        int genFromGenes2;

        for (int newGenesIndex = 0; newGenesIndex < genesLength; newGenesIndex++) {
            while (newGenes[newGenesIndex] == 0) {
                genFromGenes2 = genotype2[genes2Index];
                if (!usedGenes[genFromGenes2 - 1]) {
                    newGenes[newGenesIndex] = genFromGenes2;
                    usedGenes[genFromGenes2 - 1] = true;
                }
                genes2Index++;
            }
        }

        return newGenes;
    }
}
