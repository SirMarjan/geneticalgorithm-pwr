package Model.Operator;

import Model.Phenotype.ICreature;
import Model.Utils.RandomProvider;
import lombok.NoArgsConstructor;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@NoArgsConstructor
public class CSelectOperatorTurniej implements ISelectOperator {
    protected final Random random = RandomProvider.getRandom();

    protected final static int DEFAULT_TOURNAMENT_SIZE = 5;
    protected int tournamentSize = DEFAULT_TOURNAMENT_SIZE;

    public CSelectOperatorTurniej(int tournamentSize) {
        this.tournamentSize = tournamentSize;
    }

    @Override
    public List<ICreature> selectCreatures(List<ICreature> population) {
        List<ICreature> newPopulation = new LinkedList<>();

        List<ICreature> tournamentCreatures = new LinkedList<>();

        while (newPopulation.size() < population.size()) {
            while (tournamentCreatures.size() < tournamentSize)
                tournamentCreatures.add(population.get(random.nextInt(population.size())));
            newPopulation.add(tournamentCreatures.stream().min(Comparator.comparingInt(ICreature::getCost)).get());
            tournamentCreatures.clear();
        }


        return newPopulation;

    }
}
