package Model.Operator;

import Model.Utils.RandomProvider;
import lombok.NoArgsConstructor;

import java.util.Random;

@NoArgsConstructor
public class CMutateOperator implements IMutateOperator {
    private final static int DEFAULT_MUTATE_NUMBER = 1;

    private final Random random = RandomProvider.getRandom();
    private int mutateNumber = DEFAULT_MUTATE_NUMBER;

    public CMutateOperator(int mutateNumber) {
        this.mutateNumber = mutateNumber;
    }

    @Override
    public int[] mutate(int[] genotype) {
        int[] resultGenotype = genotype;
        for (int mutateIndex = 0; mutateIndex < mutateNumber; mutateIndex++) {
            resultGenotype = oneMutation(resultGenotype);
        }
        return resultGenotype;
    }

    private int[] oneMutation(int[] genotype) {
        int newGenotypeSize = genotype.length;

        int swapIndex1 = random.nextInt(newGenotypeSize);
        int swapIndex2;
        do {
            swapIndex2 = random.nextInt(newGenotypeSize);
        } while (swapIndex1 == swapIndex2);

        int genToSwap = genotype[swapIndex1];
        genotype[swapIndex1] = genotype[swapIndex2];
        genotype[swapIndex2] = genToSwap;
        return genotype;
    }
}
