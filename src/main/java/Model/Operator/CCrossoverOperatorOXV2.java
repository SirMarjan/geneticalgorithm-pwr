package Model.Operator;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CCrossoverOperatorOXV2 extends CCrossoverOperatorOX {

    public CCrossoverOperatorOXV2(int chainSize) {
        super(chainSize);
    }


    @Override
    protected int[] crossAlgorithm(int[] genotype1, int[] genotype2, int subChainStart, int subChainEnd) {

        final int genesLength = genotype1.length;
        final int[] newGenes = new int[genotype1.length];

        final boolean[] usedGenes = new boolean[genesLength];
        int genFromGenes1;

        for (int newGenesIndex = subChainStart; newGenesIndex < subChainEnd; newGenesIndex++) {
            genFromGenes1 = genotype1[newGenesIndex];
            newGenes[newGenesIndex] = genFromGenes1;
            usedGenes[genFromGenes1 - 1] = true;
        }

        int genes2Index = subChainEnd;
        int genFromGenes2;

        for (int newGenesIndex = subChainEnd; newGenesIndex != subChainStart; newGenesIndex = (newGenesIndex + 1) % genesLength) {
            while (newGenes[newGenesIndex] == 0) {
                genFromGenes2 = genotype2[genes2Index];
                if (!usedGenes[genFromGenes2 - 1]) {
                    newGenes[newGenesIndex] = genFromGenes2;
                    usedGenes[genFromGenes2 - 1] = true;
                }
                genes2Index = (genes2Index + 1) % genesLength;
            }
        }

        return newGenes;
    }
}
