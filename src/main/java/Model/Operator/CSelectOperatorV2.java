package Model.Operator;

import Model.Phenotype.ICreature;
import Model.Utils.RandomProvider;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class CSelectOperatorV2 implements ISelectOperator {

    private final Random random = RandomProvider.getRandom();

    @Override
    public List<ICreature> selectCreatures(List<ICreature> population) {
        List<ICreature> sortedPopulation = new LinkedList<>(population);
        sortedPopulation.sort(Comparator.comparingInt(ICreature::getCost));
        sortedPopulation = sortedPopulation.subList(0, sortedPopulation.size() / 2);

        while (sortedPopulation.size() < population.size())
            sortedPopulation.add(population.get(random.nextInt(population.size())));


        return sortedPopulation;
    }
}
