package Model.Operator;

public interface IMutateOperator {
    int[] mutate(int[] genotype);
}
