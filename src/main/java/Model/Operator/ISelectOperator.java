package Model.Operator;

import Model.Phenotype.ICreature;

import java.util.List;

public interface ISelectOperator {
    List<ICreature> selectCreatures(List<ICreature> population);
}
