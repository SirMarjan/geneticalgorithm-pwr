package Model.Operator;

import java.util.List;

public interface ICrossoverOperator {
    List<int[]> cross(int[] parentsOneGenotype, int[] parentTwoGenotype);
}
