package Model.Operator;

import Model.Phenotype.ICreature;
import Model.Utils.RandomProvider;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

public class CSelectOperatorRoulette implements ISelectOperator {
    protected Random random= RandomProvider.getRandom();

    private final double MULTIPLIER;

    public CSelectOperatorRoulette(double multiplier) {
        MULTIPLIER = multiplier;
    }


    @Override
    public List<ICreature> selectCreatures(List<ICreature> population) {
        List<CreatureWrapper> resultCreatures=wrapCreatures(population);
        resultCreatures=sortCreatureWrapper(resultCreatures);

        long costSum=calcSumCost(resultCreatures);
        long[] rouletteNumbers=randRouletteNumbers(population.size(),costSum);

        resultCreatures=getRouletteCreatures(resultCreatures,rouletteNumbers);

        return resultCreatures.stream().map(CreatureWrapper::getCreature).collect(Collectors.toList());
    }

    private List<CreatureWrapper> wrapCreatures( List<ICreature> creatureList ){
        return creatureList.stream()
                .map(iCreature -> new CreatureWrapper(iCreature, costCalc(iCreature)))
                .collect(Collectors.toList());
    }

    private long costCalc( ICreature iCreature ){
        return Math.round(MULTIPLIER/(double) iCreature.getCost());
    }

    private List<CreatureWrapper> sortCreatureWrapper( List<CreatureWrapper> creatureWrapperList ){
        List<CreatureWrapper> sortedCreatureWrapperList = new LinkedList<>(creatureWrapperList);
        sortedCreatureWrapperList.sort((c1,c2)->Double.compare(c2.getCost(),c1.getCost()));
        return sortedCreatureWrapperList;
    }

    private long calcSumCost( List<CreatureWrapper> creatureWrapperList ){
        long sum=0;
        for (CreatureWrapper creatureWrapper:creatureWrapperList){
            sum+=creatureWrapper.getCost();
        }
        return sum;
    }

    private long[] randRouletteNumbers( int size, long sum ){
        final long[] rouletteNumbers=new long[size];
        for (int numbersGenerated=0;numbersGenerated<size;numbersGenerated++){
            rouletteNumbers[numbersGenerated]=Math.round(random.nextDouble()*sum);
        }
        Arrays.sort(rouletteNumbers);
        return rouletteNumbers;
    }

    private List<CreatureWrapper> getRouletteCreatures( List<CreatureWrapper> sortedCreatures, long[] rouletteNumbers ){


        long currentRouletteNumber=0;
        int sortedCreaturesIndex;
        final int sortedCreaturesSize=sortedCreatures.size();
        int rouletteNumbersIndex=0;
        final int rouletteNumbersSize=rouletteNumbers.length;

        List<CreatureWrapper> creatureWrapperResultList=new ArrayList<>(rouletteNumbersSize);
        CreatureWrapper currentCreature;

        for (sortedCreaturesIndex=0;sortedCreaturesIndex<sortedCreaturesSize && rouletteNumbersIndex<rouletteNumbersSize ;sortedCreaturesIndex++){
            currentCreature=sortedCreatures.get(sortedCreaturesIndex);
            currentRouletteNumber+=currentCreature.getCost();
            while (rouletteNumbersIndex<rouletteNumbersSize && rouletteNumbers[rouletteNumbersIndex]-currentRouletteNumber<=0){
                rouletteNumbersIndex++;
                creatureWrapperResultList.add(currentCreature);
            }
        }

        while (creatureWrapperResultList.size()<sortedCreaturesSize){
            creatureWrapperResultList.add(sortedCreatures.get(sortedCreaturesSize-1));
            System.err.println("Zaookrąglanie problem");
        }

        return creatureWrapperResultList;


    }

    @AllArgsConstructor
    private static class CreatureWrapper{
        @Getter private ICreature creature;
        @Getter private long cost;
    }
}
