package Model.Operator;

import Model.Phenotype.ICreature;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class CSelectOperatorNoSelect implements ISelectOperator {

    @Override
    public List<ICreature> selectCreatures(List<ICreature> population) {
        return new LinkedList<>(population);
    }
}
