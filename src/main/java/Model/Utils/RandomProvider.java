package Model.Utils;

import java.util.Random;

public class RandomProvider {
    private final static Random random;

    public static Random getRandom() {
        return random;
    }

    static {
        random = new Random();
    }
}
