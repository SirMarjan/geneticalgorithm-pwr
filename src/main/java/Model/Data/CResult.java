package Model.Data;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CResult {
    @Getter
    private final List<SGenerationStatistic> generationStatistics = new LinkedList<>();

    public List<SStatistic> getStatisticList() {
        return generationStatistics.stream().map(lazyStatistic -> {
            int min = Integer.MAX_VALUE;
            int max = Integer.MIN_VALUE;
            long sum = 0;

            for (int cost : lazyStatistic.costArray) {
                if (cost < min)
                    min = cost;
                if (cost > max)
                    max = cost;
                sum += cost;
            }
            return new SStatistic(lazyStatistic.generation,
                    min,
                    max,
                    Math.toIntExact(sum / lazyStatistic.costArray.length),
                    Math.toIntExact(lazyStatistic.time) );
        }).collect(Collectors.toList());

    }

    public void addStatistic(int generation, int[] costArray, long time) {
        generationStatistics.add(new SGenerationStatistic(generation, costArray, time));

    }

    @AllArgsConstructor
    public static class SStatistic {
        public final int generation;
        public final int minCost;
        public final int maxCost;
        public final int avgCost;
        public final int time;

        public String toCSV() {
            return String.valueOf(generation) +
                    ',' +
                    minCost +
                    ',' +
                    maxCost +
                    ',' +
                    avgCost +
                    ',' +
                    time;
        }
    }

    @AllArgsConstructor
    public static class SGenerationStatistic {
        public final int generation;
        public final int[] costArray;
        public final long time;
    }


}
