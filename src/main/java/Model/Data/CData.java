package Model.Data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class CData {
    private final int matrixSize;
    private final int[][] distanceMatrix;
    private final int[][] flowMatrix;

    public CData(int numberOfLocations, int[][] distanceMatrix, int[][] flowMatrix) {
        this.matrixSize = numberOfLocations;
        this.distanceMatrix = distanceMatrix;
        this.flowMatrix = flowMatrix;
    }

    public int getMatrixSize() {
        return matrixSize;
    }

    public int[][] getDistanceMatrix() {
        return distanceMatrix;
    }

    public int[][] getFlowMatrix() {
        return flowMatrix;
    }

    public static CData loadData(String fileName) throws IOException {
        int numberOfLocations;
        int[][] distanceMatrix;
        int[][] flowMatrix;
        CData loadedDate = null;

        try (BufferedReader bufferedDatReader = new BufferedReader(new FileReader(fileName))) {
            Scanner datScanner = new Scanner(bufferedDatReader);
            numberOfLocations = datScanner.nextInt();

            if (numberOfLocations <= 0)
                throw new ArrayIndexOutOfBoundsException("Wielkość macieży < 0");

            distanceMatrix = new int[numberOfLocations][numberOfLocations];
            flowMatrix = new int[numberOfLocations][numberOfLocations];

            for (int distanceMatrixY = 0; distanceMatrixY < numberOfLocations; distanceMatrixY++) {
                for (int distanceMatrixX = 0; distanceMatrixX < numberOfLocations; distanceMatrixX++) {
                    distanceMatrix[distanceMatrixX][distanceMatrixY] = datScanner.nextInt();
                }
            }

            for (int flowMatrixY = 0; flowMatrixY < numberOfLocations; flowMatrixY++) {
                for (int flowMatrixX = 0; flowMatrixX < numberOfLocations; flowMatrixX++) {
                    flowMatrix[flowMatrixX][flowMatrixY] = datScanner.nextInt();
                }
            }

            loadedDate = new CData(numberOfLocations, distanceMatrix, flowMatrix);


        }

        return loadedDate;
    }
}
