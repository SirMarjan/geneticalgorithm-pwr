package Model.Phenotype;

import Model.Utils.RandomProvider;
import lombok.NoArgsConstructor;

import java.util.Random;


@NoArgsConstructor
public class CGenerateGenotype implements IGenerateGenotype {
    private final static Random random = RandomProvider.getRandom();


    @Override
    public int[] generateGenotype(int genotypeSize) {
        if (genotypeSize < 0)
            throw new NegativeArraySizeException();

        return randomArray(genotypeSize);
    }

    private int[] randomArray(int genotypeSize) {
        int[] generatedArray = generateReverseArray(genotypeSize);
        shuffleArray(generatedArray);
        return generatedArray;
    }

    private int[] generateReverseArray(int genotypeSize) {
        int[] resultArray = new int[genotypeSize];

        for (int arrayIndex = genotypeSize - 1; arrayIndex >= 0; arrayIndex--) {
            resultArray[arrayIndex] = arrayIndex + 1;
        }
        return resultArray;
    }

    private void shuffleArray(int[] array) {
        for (int arrayIndex = array.length - 1; arrayIndex > 0; arrayIndex--) {
            int index = random.nextInt(arrayIndex + 1);
            int a = array[index];
            array[index] = array[arrayIndex];
            array[arrayIndex] = a;
        }
    }

}
