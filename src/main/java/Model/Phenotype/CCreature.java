package Model.Phenotype;

import Model.Data.CData;

public class CCreature implements ICreature {
    private final static int LAZY_COST_UNASSIGNED = -1;
    private final CData data;

    private final int[] genotype;
    private int lazyCost = LAZY_COST_UNASSIGNED;

    @Override
    public int[] getGenotype() {
        return genotype.clone();
    }

    @Override
    public int getCost() {
        if (lazyCost == LAZY_COST_UNASSIGNED)
            lazyCost = calcCost();

        return lazyCost;
    }

    private CCreature(CData data, int[] genotype) {
        this.data = data;
        this.genotype = genotype;
    }

    private int calcCost() {
        int result = 0;
        int distanceValue;
        int flowValue;

        final int matrixLength = genotype.length;
        final int[][] distanceMatrix = data.getDistanceMatrix();
        final int[][] flowMatrix = data.getFlowMatrix();

        for (int matrixY = 0; matrixY < matrixLength; matrixY++) {
            for (int matrixX = 0; matrixX < matrixLength; matrixX++) {
                distanceValue = distanceMatrix[matrixY][matrixX];
                flowValue = flowMatrix[genotype[matrixY] - 1][genotype[matrixX] - 1];
                result += distanceValue * flowValue;
            }
        }
        return result;
    }

    public static class CICreatureFabric {
        private final CData creaturesCData;

        private CICreatureFabric(CData data) {
            creaturesCData = data;
        }

        public static CICreatureFabric getCCreatureFabric(CData data) {
            return new CICreatureFabric(data);
        }

        public ICreature createNewICreature(int[] genotype) {
            return new CCreature(creaturesCData, genotype);
        }
    }

}
