package Model.Phenotype;

public interface IGenerateGenotype {
    int[] generateGenotype(int genotypeSize);
}
