package Model.Phenotype;

public interface ICreature {
    int[] getGenotype();

    int getCost();
}
